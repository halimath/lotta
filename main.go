package main

import (
	"bitbucket.org/halimath/lotta/cmd"
)

func main() {
	cmd.Execute()
}
