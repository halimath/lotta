package cmd

import (
	"fmt"
	"os"

	"bitbucket.org/halimath/lotta/internal/sampler"

	"github.com/spf13/cobra"
)

var skipTLSHostVerification bool
var userAgent string
var authorizationHeader string
var disableKeepAlive bool

var rootCmd = &cobra.Command{
	Use:   "lotta",
	Short: "lotta is a HTTP load testing tool",
	Long:  `A simple to use yet powerful and versatile load testing tool for HTTP`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(cmd.UsageString())
	},
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&skipTLSHostVerification, "skip-tls-host-verification", "t", false, "Skip TLS host verification checks")
	rootCmd.PersistentFlags().BoolVarP(&disableKeepAlive, "disable-keep-alives", "k", false, "Disable HTTP keep-alives")
	rootCmd.PersistentFlags().StringVarP(&userAgent, "user-agent", "u", "lotta/0.1.0", "HTTP User Agent")
	rootCmd.PersistentFlags().StringVarP(&authorizationHeader, "authorization-header", "a", "", "HTTP authorization header to send")
}

// urlSampleConfigurationFromFlags creates a sampler.URLSamplerConfiguration using the CLI flags/ default values
func urlSampleConfigurationFromFlags() sampler.URLSamplerConfiguration {
	return sampler.URLSamplerConfiguration{
		DisableTLSVerification: skipTLSHostVerification,
		DisableKeepAlives:      disableKeepAlive,
		UserAgent:              userAgent,
		Authorization: 			authorizationHeader,
	}
}

func banner() string {
	return fmt.Sprintf(`
  _         _     _          
 | |  ___  | |_  | |_   __ _ 
 | | / _ \ |  _| |  _| / _' |
 |_| \___/  \__|  \__| \__,_|
 version %s                             	
	`, Version)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
