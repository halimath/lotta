package cmd

import (
	"fmt"
	"net/url"
	"sort"
	"time"

	"bitbucket.org/halimath/lotta/internal/request"
	"bitbucket.org/halimath/lotta/internal/sampler"
	"bitbucket.org/halimath/lotta/internal/stats"
	"github.com/spf13/cobra"
)

var (
	benchmarkCommandIterations       int
	benchmarkCommandConcurrencyLevel int
)

var benchmarkCommand = &cobra.Command{
	Use:     "benchmark",
	Aliases: []string{"bench", "b"},
	Short:   "Benchmark a set of static URL",
	Long:    "Run a load test against a static set of HTTP(S) URL",
	Args:    cobra.MinimumNArgs(1),

	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(banner())

		urlSampler := sampler.NewURLSampler(urlSampleConfigurationFromFlags())

		series := stats.NewSeries("urls")

		requests := make(chan *request.FinishedRequest, 10)

		urlSampler.SampleURLs(sampler.SamplingOptions{
			Iterations:       benchmarkCommandIterations,
			ConcurrencyLevel: benchmarkCommandConcurrencyLevel,
			Requests:         requests,
		}, args...)

		fmt.Printf("Testing with %d threads, %d iterations per thread:\n", benchmarkCommandConcurrencyLevel, benchmarkCommandIterations)
		for _, u := range args {
			fmt.Printf("  - %s\n", u)
		}

		ticker := time.NewTicker(1 * time.Second)

	waitLoop:
		for {
			select {
			case <-ticker.C:
				stats := series.Stats()
				fmt.Printf("\r%5d/%5d requests; %5d errors; %8.4f reqs/s; %8.2f Kb/s",
					stats.NumberOfRequests(), len(args)*benchmarkCommandIterations*benchmarkCommandConcurrencyLevel, stats.NumberOfErrors(), stats.RequestsPerSecond(), stats.BytesPerSecond()/1024)
			case req, ok := <-requests:
				if !ok {
					break waitLoop
				}
				series.AddRequest(req)
			}
		}

		fmt.Printf("\r%100s\n", "")

		fmt.Printf("%60s %5s %5s %9s %9s %9s %9s %9s %9s %9s %12s %10s\n", "URL", "reqs", "errs", "Min", "Avg", "Med", "80%", "90%", "99%", "Max", "Reqs/s", "Kb/s")

		statsByURL := series.StatsByURL()
		sort.Slice(statsByURL, func(i, j int) bool {
			return statsByURL[i].Label < statsByURL[j].Label
		})

		for _, stats := range statsByURL {
			var label string
			parsedURL, err := url.Parse(stats.Label)
			if err != nil {
				label = stats.Label
			} else {
				label = parsedURL.Host
			}

			fmt.Printf("%60s %5d %5d %8.3fs %8.3fs %8.3fs %8.3fs %8.3fs %8.3fs %8.3fs %11.4f %11.2f\n",
				label, stats.NumberOfRequests(), stats.NumberOfErrors(), stats.MinTotalTime().Seconds(), stats.AverageTotalTime().Seconds(), stats.MedianTotalTime().Seconds(),
				stats.TotalTimePercentile(0.8).Seconds(), stats.TotalTimePercentile(0.9).Seconds(), stats.TotalTimePercentile(0.99).Seconds(),
				stats.MaxTotalTime().Seconds(), stats.RequestsPerSecond(), stats.BytesPerSecond()/1024)
		}
	},
}

func init() {
	benchmarkCommand.Flags().IntVarP(&benchmarkCommandIterations, "iterations", "i", 10, "Number of iterations per thread")
	benchmarkCommand.Flags().IntVarP(&benchmarkCommandConcurrencyLevel, "concurrency-level", "c", 1, "Number of parallel thread")
	rootCmd.AddCommand(benchmarkCommand)
}
