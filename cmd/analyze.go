package cmd

import (
	"fmt"
	"net/url"
	"os"
	"text/template"
	"time"

	"bitbucket.org/halimath/lotta/internal/request"
	"bitbucket.org/halimath/lotta/internal/sampler"
	"github.com/spf13/cobra"
)

var analyzeCommand = &cobra.Command{
	Use:     "analyze",
	Aliases: []string{"ana", "a"},
	Short:   "Analyze a single URL",
	Long:    "Analyze headers and detailed request timings of a single URL",
	Args:    cobra.ExactArgs(1),

	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(banner())

		urlSampler := sampler.NewURLSampler(urlSampleConfigurationFromFlags())

		requests := make(chan *request.FinishedRequest, 10)

		urlSampler.SampleURLs(sampler.SamplingOptions{
			Iterations:       1,
			ConcurrencyLevel: 1,
			Requests:         requests,
		}, args[0])

		fmt.Printf("Analyzing %s\n", args[0])

		finishedRequest := <-requests

		parsedURL, _ := url.Parse(args[0])

		analyzeCommandOutputTemplate.Execute(os.Stdout, analyzeCommandOutputTemplateModel{
			ParsedURL: parsedURL,
			Request:   finishedRequest,
		})
	},
}

func init() {
	rootCmd.AddCommand(analyzeCommand)
}

type analyzeCommandOutputTemplateModel struct {
	ParsedURL *url.URL
	Request   *request.FinishedRequest
}

var analyzeCommandOutputTemplateFunctionsMap = template.FuncMap{
	"millis": func(t time.Duration) string {
		return fmt.Sprintf("%7.2f ms", float32(t)/float32(time.Millisecond))
	},
}

const analyzeCommandOutputTemplateString = `
Protocol:              {{.ParsedURL.Scheme}}
Host:                  {{.ParsedURL.Host}}
Path:                  {{.ParsedURL.Path}}
QueryString:           {{.ParsedURL.RawQuery}}

{{if .Request.Error}}
Error:                 {{.Request.Error}}
{{else}}
Response status        {{.Request.StatusCode}}

Content Length:        {{.Request.ContentLength}} bytes
Content Type:          {{.Request.ContentType}}

Header:
{{.Request.Header}}

DNS Time:              {{millis .Request.TimingDetails.DNSTime}}
TCP Connect Time:      {{millis .Request.TimingDetails.TCPConnectTime}}
TLS Time:              {{millis .Request.TimingDetails.TLSTime}}
Send Request Time:     {{millis .Request.TimingDetails.SendRequestTime}}
Time To First Byte:    {{millis .Request.TimingDetails.TimeToFirstByte}}
Content Transfer Time: {{millis .Request.TimingDetails.ContentTransferTime}}
Total Time:            {{millis .Request.TimingDetails.TotalTime}}
{{end}}
`

var analyzeCommandOutputTemplate = template.Must(
	template.New("analyzeCommandOutput").
		Funcs(analyzeCommandOutputTemplateFunctionsMap).
		Parse(analyzeCommandOutputTemplateString))
