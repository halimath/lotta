package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

const Version = "0.4.0"

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:     "version",
	Aliases: []string{"v"},
	Short:   "Print the version number of lotta",
	Long:    "Print the version number of lotta",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(banner())
		fmt.Printf("Copyright (c) 2018 Alexander Metzner.\nPublished under the terms of the Apache License 2.0\n\n")
	},
}
