package cmd

import (
	"fmt"
	"net/url"
	"sort"
	"time"

	"bitbucket.org/halimath/lotta/internal/request"
	"bitbucket.org/halimath/lotta/internal/sampler"
	"bitbucket.org/halimath/lotta/internal/stats"
	"github.com/spf13/cobra"
)

var profileCommandPercentile int
var profileCommandNumberOfIterations int

var profileCommand = &cobra.Command{
	Use:     "profile",
	Aliases: []string{"pro", "p"},
	Short:   "Profile timings for a set of URLs",
	Long:    "Profile timing details and compare them statistically for a set of URLs",
	Args:    cobra.MinimumNArgs(1),

	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(banner())

		urlSampler := sampler.NewURLSampler(urlSampleConfigurationFromFlags())

		requests := make(chan *request.FinishedRequest, 10)

		urlSampler.SampleURLs(sampler.SamplingOptions{
			Iterations:       profileCommandNumberOfIterations,
			ConcurrencyLevel: 1,
			Requests:         requests,
		}, args...)

		fmt.Printf("Profiling URLs\n")

		series := stats.NewSeries("urls")

		ticker := time.NewTicker(1 * time.Second)

	waitLoop:
		for {
			select {
			case <-ticker.C:
				stats := series.Stats()
				fmt.Printf("\r%5d/%5d requests; %5d errors",
					stats.NumberOfRequests(), len(args)*profileCommandNumberOfIterations, stats.NumberOfErrors())
			case req, ok := <-requests:
				if !ok {
					break waitLoop
				}
				series.AddRequest(req)
			}
		}

		fmt.Printf("\r%100s\n", "")

		fmt.Printf("%60s %5s %5s %4s (%d%%) %4s (%d%%) %4s (%d%%) %4s (%d%%) %4s (%d%%) %4s (%d%%) %4s (%d%%)\n",
			"URL", "reqs", "errs", "tot", profileCommandPercentile, "dns", profileCommandPercentile, "con", profileCommandPercentile, "tls", profileCommandPercentile, "wrt", profileCommandPercentile, "tfb", profileCommandPercentile, "tfr", profileCommandPercentile)

		statsByURL := series.StatsByURL()
		sort.Slice(statsByURL, func(i, j int) bool {
			return statsByURL[i].Label < statsByURL[j].Label
		})

		for _, stats := range statsByURL {
			var label string
			parsedURL, err := url.Parse(stats.Label)
			if err != nil {
				label = stats.Label
			} else {
				label = parsedURL.Host
			}

			timingDetails := prepareRequestTimings(stats.FinishedRequests, float32(profileCommandPercentile)/100.00)

			fmt.Printf("%60s %5d %5d %9.3fs %9.3fs %9.3fs %9.3fs %9.3fs %9.3fs %9.3fs\n",
				label,
				stats.NumberOfRequests(),
				stats.NumberOfErrors(),
				timingDetails.TotalTime.Seconds(),
				timingDetails.DNSTime.Seconds(),
				timingDetails.TCPConnectTime.Seconds(),
				timingDetails.TLSTime.Seconds(),
				timingDetails.SendRequestTime.Seconds(),
				timingDetails.TimeToFirstByte.Seconds(),
				timingDetails.ContentTransferTime.Seconds())
		}
	},
}

func init() {
	profileCommand.Flags().IntVarP(&profileCommandPercentile, "percentile", "p", 80, "The percentile to report for all timings")
	profileCommand.Flags().IntVarP(&profileCommandNumberOfIterations, "iterations", "i", 10, "The number of iterations for every URL")
	rootCmd.AddCommand(profileCommand)
}

func prepareRequestTimings(reqs stats.FinishedRequests, percentile float32) request.TimingDetails {
	var timingDetails request.TimingDetails

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.TotalTime < reqs[j].TimingDetails.TotalTime
	})
	timingDetails.TotalTime = reqs[int(float32(len(reqs))*percentile)].TimingDetails.TotalTime

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.DNSTime < reqs[j].TimingDetails.DNSTime
	})
	timingDetails.DNSTime = reqs[int(float32(len(reqs))*percentile)].TimingDetails.DNSTime

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.TCPConnectTime < reqs[j].TimingDetails.TCPConnectTime
	})
	timingDetails.TCPConnectTime = reqs[int(float32(len(reqs))*percentile)].TimingDetails.TCPConnectTime

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.TLSTime < reqs[j].TimingDetails.TLSTime
	})
	timingDetails.TLSTime = reqs[int(float32(len(reqs))*percentile)].TimingDetails.TLSTime

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.SendRequestTime < reqs[j].TimingDetails.SendRequestTime
	})
	timingDetails.SendRequestTime = reqs[int(float32(len(reqs))*percentile)].TimingDetails.SendRequestTime

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.TimeToFirstByte < reqs[j].TimingDetails.TimeToFirstByte
	})
	timingDetails.TimeToFirstByte = reqs[int(float32(len(reqs))*percentile)].TimingDetails.TimeToFirstByte

	sort.Slice(reqs, func(i, j int) bool {
		return reqs[i].TimingDetails.ContentTransferTime < reqs[j].TimingDetails.ContentTransferTime
	})
	timingDetails.ContentTransferTime = reqs[int(float32(len(reqs))*percentile)].TimingDetails.ContentTransferTime

	return timingDetails
}

// type analyzeCommandOutputTemplateModel struct {
// 	ParsedURL *url.URL
// 	Request   *request.FinishedRequest
// }

// var analyzeCommandOutputTemplateFunctionsMap = template.FuncMap{
// 	"millis": func(t time.Duration) string {
// 		return fmt.Sprintf("%7.2f ms", float32(t)/float32(time.Millisecond))
// 	},
// }

// const analyzeCommandOutputTemplateString = `
// Protocol:              {{.ParsedURL.Scheme}}
// Host:                  {{.ParsedURL.Host}}
// Path:                  {{.ParsedURL.Path}}
// QueryString:           {{.ParsedURL.RawQuery}}

// {{if .Request.Error}}
// Error:                 {{.Request.Error}}
// {{else}}
// Response status        {{.Request.StatusCode}}

// Content Length:        {{.Request.ContentLength}} bytes
// Content Type:          {{.Request.ContentType}}

// Header:
// {{.Request.Header}}

// DNS Time:              {{millis .Request.TimingDetails.DNSTime}}
// TCP Connect Time:      {{millis .Request.TimingDetails.TCPConnectTime}}
// TLS Time:              {{millis .Request.TimingDetails.TLSTime}}
// Send Request Time:     {{millis .Request.TimingDetails.SendRequestTime}}
// Time To First Byte:    {{millis .Request.TimingDetails.TimeToFirstByte}}
// Content Transfer Time: {{millis .Request.TimingDetails.ContentTransferTime}}
// Total Time:            {{millis .Request.TimingDetails.TotalTime}}
// {{end}}
// `

// var analyzeCommandOutputTemplate = template.Must(
// 	template.New("analyzeCommandOutput").
// 		Funcs(analyzeCommandOutputTemplateFunctionsMap).
// 		Parse(analyzeCommandOutputTemplateString))
