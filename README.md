# lotta

A HTTP load testing and performance analysis tool.

## Usage

Lotta supports the following commands and global options.


| Option | Description |
|--------|-------------|
| `-a`, `--authorization-header string` | HTTP authorization header to send |
| `-k`, `--disable-keep-alives` | Disable HTTP keep-alives |
| `-t`, `--skip-tls-host-verification` | Skip TLS host verification checks |
| `-u`, `--user-agent string` | HTTP User Agent (default "lotta/0.1.0") |

### Analyze

Analyzes a HTPP connection to the given URL and prints various details about the connection.

```
$ lotta analyze http://httpbin.org/headers

  _         _     _
 | |  ___  | |_  | |_   __ _ 
 | | / _ \ |  _| |  _| / _' |
 |_| \___/  \__|  \__| \__,_|
 version 0.4.0

Analyzing http://httpbin.org/headers

Protocol:              http
Host:                  httpbin.org
Path:                  /headers
QueryString:


Response status        200

Content Length:        148 bytes
Content Type:          application/json

Header:
Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: *
Date: Thu, 08 Aug 2019 20:32:30 GMT
Referrer-Policy: no-referrer-when-downgrade
X-Xss-Protection: 1; mode=block
Connection: keep-alive
Content-Type: application/json
Server: nginx
X-Content-Type-Options: nosniff
X-Frame-Options: DENY


DNS Time:                20.00 ms
TCP Connect Time:       123.02 ms
TLS Time:                 0.00 ms
Send Request Time:        2.06 ms
Time To First Byte:     111.47 ms
Content Transfer Time:    7.26 ms
Total Time:             271.80 ms
```

### Benchmark

Benchmarks the given URL based on multiple requests. This is similar to what Apache Bench does.

| Option | Description |
|--------|-------------|
| `-c`, `--concurrency-level int` | Number of parallel thread (default 1) |
| `-i`, `--iterations int` | Number of iterations per thread (default 10) |

```
$ lotta benchmark -c 5 -i 100 http://httpbin.org/headers

  _         _     _
 | |  ___  | |_  | |_   __ _
 | | / _ \ |  _| |  _| / _' |
 |_| \___/  \__|  \__| \__,_|
 version 0.4.0

Testing with 5 threads, 100 iterations per thread:
  - http://httpbin.org/headers

                                                         URL  reqs  errs       Min       Avg       Med       80%       90%       99%       Max       Reqs/s       Kb/s
                                                 httpbin.org   500     0    0.094s    0.112s    0.110s    0.114s    0.120s    0.248s    0.252s     43.9051        6.35

```

### Profile

Profiles a given set of URLs and measures various timings.

| Option | Description |
| `-i`, `--iterations int` | The number of iterations for every URL (default 10) |
| `-p`, `--percentile int` | The percentile to report for all timings (default 80) |

```
$ lotta profile -k https://httpbin.org/headers

  _         _     _
 | |  ___  | |_  | |_   __ _
 | | / _ \ |  _| |  _| / _' |
 |_| \___/  \__|  \__| \__,_|
 version 0.4.0

Profiling URLs

                                                         URL  reqs  errs  tot (80%)  dns (80%)  con (80%)  tls (80%)  wrt (80%)  tfb (80%)  tfr (80%)
                                                 httpbin.org    10     0     0.482s     0.003s     0.122s     0.238s     0.012s     0.127s     0.010s```

The values have the following meaning

| Abbreviation | Description |
|--------------|-------------|
| `reqs` | Number of requests |
| `errs` | Number of errors |
| `tot` | Total time in seconds |
| `dns` | Time needed to resolve the host name via DNS in seconds |
| `con` | TCP handshake time - to get usefull values here you should disable keep alive |
| `tls` | TLS handshake time - to get usefull values here you should disable keep alive |
| `wrt` | Time to write the request |
| `tfb` | Time to first byte - latency before any reponse data is received |
| `tfr` | Content transfer time - time needed to download the content |
```

# License

```
Copyright 2019 Alexander Metzner.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```