package stats

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/halimath/lotta/internal/request"
)

const (
	numberOfFinishedRequest = 10
	delayBetweenRequests = time.Second
)

func assertEqual(t *testing.T, message string, actual, expected interface{}) {
	if expected != actual {
		t.Errorf("%s %v != %v", message, expected, actual)
	}
}

func TestFinishedRequestsGlobalTiming(t *testing.T) {
	start := time.Now()

	reqs := createFinishedRequests(start)
	reqs[0].Error = fmt.Errorf("Failed")

	
	finished := start.Add(delayBetweenRequests * time.Duration(numberOfFinishedRequest - 1)).Add(time.Duration(numberOfFinishedRequest - 1) * time.Millisecond)
	
	totalTime := time.Duration(0)
	for i := 0; i < numberOfFinishedRequest; i++ {
		totalTime += time.Duration(i) * time.Millisecond
	}
	
	assertEqual(t, "Started", reqs.Started(), start)
	assertEqual(t, "Finished", reqs.Finished(), finished)

	assertEqual(t, "NumberOfRequests", reqs.NumberOfRequests(), numberOfFinishedRequest)
	assertEqual(t, "NumberOfErrors", reqs.NumberOfErrors(), 1)
	assertEqual(t, "NumberOfKeepAliveRequests", reqs.NumberOfKeepAliveRequests(), numberOfFinishedRequest)
	
	assertEqual(t, "TotalBytes", reqs.TotalBytes(), int64(numberOfFinishedRequest * 100))

	assertEqual(t, "TotalTime", reqs.TotalTime(), totalTime)
	assertEqual(t, "TotalDuration", reqs.TotalDuration(), finished.Sub(start))
	assertEqual(t, "MinTotalTime", reqs.MinTotalTime(), time.Duration(0))
	assertEqual(t, "AverageTotalTime", reqs.AverageTotalTime(), totalTime / numberOfFinishedRequest)
	assertEqual(t, "MedianTotalTime", reqs.MedianTotalTime(), time.Duration(numberOfFinishedRequest / 2) * time.Millisecond)
	assertEqual(t, "MaxTotalTime", reqs.MaxTotalTime(), time.Duration(numberOfFinishedRequest - 1) * time.Millisecond)

	assertEqual(t, "RequestsPerSecond", reqs.RequestsPerSecond(), float32(numberOfFinishedRequest) / float32(finished.Sub(start).Seconds()))
	assertEqual(t, "BytesPerSecond", reqs.BytesPerSecond(), float32(numberOfFinishedRequest * 100) / float32(finished.Sub(start).Seconds()))
}

func createFinishedRequests(start time.Time) FinishedRequests {
	reqs := make([]*request.FinishedRequest, 0, 10)

	for i := 0; i < numberOfFinishedRequest; i++ {
		reqs = append(reqs, &request.FinishedRequest{
			URL:           "/test/url",
			Error:         nil,
			StatusCode:    200,
			Started:       start,
			Finished:      start.Add(time.Duration(i) * time.Millisecond),
			ContentLength: 100,
			Header:        nil,
			Reused:        true,
			TimingDetails: request.TimingDetails{
				TotalTime:           time.Duration(i) * time.Millisecond,
				DNSTime:             time.Millisecond,
				TCPConnectTime:      time.Millisecond,
				TLSTime:             time.Millisecond,
				SendRequestTime:     time.Millisecond,
				TimeToFirstByte:     time.Millisecond,
				ContentTransferTime: time.Millisecond,
			},
		})

		start = start.Add(delayBetweenRequests)
	}

	return FinishedRequests(reqs)
}
