// Package stats provides types and functions to handle statistics of executed requests incl.
// timing, errors and content
package stats

import (
	"fmt"
	"sort"
	"time"

	"bitbucket.org/halimath/lotta/internal/request"
)

// FinishedRequests represents a list of finished requests
// Implements sort.Interface which allows finished requests to be sorted by total time
type FinishedRequests []*request.FinishedRequest

// Returns the number of samples
// Needed by sort.Interface
func (f FinishedRequests) Len() int { return len(f) }

// Swaps two elements
// Needed by sort.Interface
func (f FinishedRequests) Swap(i, j int) { f[i], f[j] = f[j], f[i] }

// Compares two elements
// Needed by sort.Interface
func (f FinishedRequests) Less(i, j int) bool { return f[i].TotalTime() < f[j].TotalTime() }

// Started returns the first start time of all samples
func (f FinishedRequests) Started() time.Time {
	if len(f) == 0 {
		return time.Now()
	}

	started := f[0].Started

	for _, r := range f[1:] {
		if r.Started.Before(started) {
			started = r.Started
		}
	}

	return started
}

// Finished returns the last finished time of all samples
func (f FinishedRequests) Finished() time.Time {
	if len(f) == 0 {
		return time.Now()
	}

	finished := f[0].Finished

	for _, r := range f[1:] {
		if r.Finished.After(finished) {
			finished = r.Finished
		}
	}

	return finished
}

// NumberOfErrors returns the number of samples marked as errornous
func (f FinishedRequests) NumberOfErrors() (errors int) {
	for _, r := range f {
		if r.IsError() {
			errors++
		}
	}

	return
}

// NumberOfKeepAliveRequests returns the number of requests that reused an existing TCP connection
func (f FinishedRequests) NumberOfKeepAliveRequests() (count int) {
	for _, r := range f {
		if r.Reused {
			count++
		}
	}

	return
}

// TotalDuration returns the overall duration of all samples
func (f FinishedRequests) TotalDuration() time.Duration {
	return f.Finished().Sub(f.Started())
}

// TotalTime returns the total time
func (f FinishedRequests) TotalTime() time.Duration {
	var t time.Duration

	for _, sample := range f {
		t += sample.TotalTime()
	}

	return t
}

// RequestsPerSecond returns the number of requests per second
func (f FinishedRequests) RequestsPerSecond() float32 {
	return float32(f.NumberOfRequests()) / float32(f.TotalDuration().Seconds())
}

// TotalBytes returns the total bytes received for the samples
func (f FinishedRequests) TotalBytes() int64 {
	var size int64

	for _, sample := range f {
		size += sample.ContentLength
	}

	return size
}

// BytesPerSecond returns the number of requests per second
func (f FinishedRequests) BytesPerSecond() float32 {
	return float32(f.TotalBytes()) / float32(f.TotalDuration().Seconds())
}

// NumberOfRequests returns the number of samples.
func (f FinishedRequests) NumberOfRequests() int {
	return f.Len()
}

// MinTotalTime returns the minimum total time.
func (f FinishedRequests) MinTotalTime() time.Duration {
	if len(f) == 0 {
		return 0
	}

	return f[0].TotalTime()
}

// TotalTimePercentile returns the given total time percentile
// percentile must be between 0 and 1.0
func (f FinishedRequests) TotalTimePercentile(percentile float32) time.Duration {
	if percentile < 0 || percentile > 1 {
		panic(fmt.Sprintf("Invalid percentile: %f", percentile))
	}

	if len(f) == 0 {
		return 0
	}

	return f[int(float32(len(f))*percentile)].TotalTime()
}

// MedianTotalTime returns the median total which is the 50% percentile
func (f FinishedRequests) MedianTotalTime() time.Duration {
	return f.TotalTimePercentile(0.5)
}

// MaxTotalTime returns the maximum total time
func (f FinishedRequests) MaxTotalTime() time.Duration {
	if len(f) == 0 {
		return 0
	}

	return f[len(f)-1].TotalTime()
}

// AverageTotalTime returns the average total time
func (f FinishedRequests) AverageTotalTime() time.Duration {
	if len(f) == 0 {
		return 0
	}

	sum := f[0].TotalTime()
	for _, sample := range f[1:] {
		sum += sample.TotalTime()
	}

	return sum / time.Duration(len(f))
}

// -------------------------------------------------------------------------------------------------------

// Stats represents a labeled set of samples
type Stats struct {
	// A Label (URL most of the time) for these stats
	Label string

	// The requests
	FinishedRequests
}

// FirstNonErrorRequest returns the first finished request that was successful or nil
func (s *Stats) FirstNonErrorRequest() *request.FinishedRequest {
	for _, r := range s.FinishedRequests {
		if !r.IsError() {
			return r
		}
	}

	return nil
}

// -------------------------------------------------------------------------------------------------------

// Series represents a (potentially growing) set of samples
// Series provides operations for obtaining Stats from the samples
// using different groupings.
type Series struct {
	Label            string
	FinishedRequests []*request.FinishedRequest
}

// NewSeries is a factory for creating a new Series
func NewSeries(label string) *Series {
	return &Series{Label: label}
}

// AddRequest adds a single sample to the series
// It returns the series for invocation chaining
func (s *Series) AddRequest(req *request.FinishedRequest) *Series {
	s.FinishedRequests = append(s.FinishedRequests, req)
	return s
}

// Stats "freezes" the current samples of this series and returns a Stats instance for those samples. The series may be used to collect more samples.
func (s *Series) Stats() Stats {
	count := len(s.FinishedRequests)

	if count == 0 {
		return Stats{
			Label:            s.Label,
			FinishedRequests: make(FinishedRequests, 0),
		}
	}

	samples := make(FinishedRequests, count)

	copy(samples, s.FinishedRequests)
	sort.Sort(samples)

	return Stats{
		Label:            s.Label,
		FinishedRequests: samples,
	}
}

// StatsByURL groups all samples of the series by their URL and returns stats for each URL.
func (s *Series) StatsByURL() []Stats {
	count := len(s.FinishedRequests)

	labelToSamples := make(map[string]FinishedRequests)

	for i := 0; i < count; i++ {
		if _, ok := labelToSamples[s.FinishedRequests[i].URL]; !ok {
			labelToSamples[s.FinishedRequests[i].URL] = FinishedRequests{s.FinishedRequests[i]}
		} else {
			labelToSamples[s.FinishedRequests[i].URL] = append(labelToSamples[s.FinishedRequests[i].URL], s.FinishedRequests[i])
		}
	}

	result := make([]Stats, 0, len(labelToSamples))

	for u, samples := range labelToSamples {
		sort.Sort(samples)
		result = append(result, Stats{Label: u, FinishedRequests: samples})
	}

	return result
}
