package sampler

import (
	"crypto/tls"
	"net/http"
	"sync"

	"bitbucket.org/halimath/lotta/internal/request"
)

// URLSamplerConfiguration describes the configuration properties for an URLSampler
type URLSamplerConfiguration struct {
	// Disable TLS verfication checks (i.e. accept self-signed certificates)?
	DisableTLSVerification bool

	// Disable HTTP Keep-Alive connection pooling?
	DisableKeepAlives bool

	// The HTTP User-Agent to use
	UserAgent string

	// HTTP Authorization header to send
	Authorization string
}

// URLSampler provides methods for sampling URLs.
type URLSampler struct {
	configuration URLSamplerConfiguration
	client        *http.Client
}

// NewURLSampler creates a new URLSampler using the given configuration
func NewURLSampler(configuration URLSamplerConfiguration) *URLSampler {
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: configuration.DisableTLSVerification,
		},
		DisableKeepAlives: configuration.DisableKeepAlives,
	}
	client := &http.Client{Transport: transport}

	return &URLSampler{configuration, client}
}

// SamplingOptions collect options for sampling URLs
type SamplingOptions struct {
	// How often should each URL be samples
	Iterations int

	// Number of parallel goroutines to start per URL
	ConcurrencyLevel int

	// Channel to send finished requests to
	Requests chan<- *request.FinishedRequest
}

// SampleURLs samples the given urls each with the given options
func (s *URLSampler) SampleURLs(options SamplingOptions, urls ...string) {
	var waitGroup sync.WaitGroup

	header := make(map[string]string)
	header["Authorization"] = s.configuration.Authorization

	for _, url := range urls {
		for i := 0; i < options.ConcurrencyLevel; i++ {
			waitGroup.Add(1)

			go func(url string) {
				defer waitGroup.Done()

				for i := 0; i < options.Iterations; i++ {
					options.Requests <- request.ExecuteGet(url, header, s.client)
				}
			}(url)
		}
	}

	go func() {
		waitGroup.Wait()
		close(options.Requests)
	}()
}
