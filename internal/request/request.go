// Package request provides types and functions to describe executed requests and their attributes
package request

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptrace"
	"time"
)

// Header contains the HTTP response headers for a request
// Header names are canonicalized, i.e. "Content-Length"
type Header map[string][]string

func (h Header) String() string {
	var buf bytes.Buffer

	for header, values := range h {
		for _, value := range values {
			fmt.Fprintf(&buf, "%s: %s\n", header, value)
		}
	}

	return buf.String()
}

// --

type TimingDetails struct {
	// The total time needed to execute the request
	TotalTime time.Duration

	// The time needed to resolve the host name via DNS
	DNSTime time.Duration

	// The time needed to establish a TCP connection
	TCPConnectTime time.Duration

	// The time needed to perform the TLS handshake
	TLSTime time.Duration

	// The time needed to write the request
	SendRequestTime time.Duration

	// The time waited for the first response byte
	TimeToFirstByte time.Duration

	// The time needed to transfer all content
	ContentTransferTime time.Duration
}

// --

// FinishedRequest represents a single sample of an HTTP(S) request.
// It includes status and timing information
type FinishedRequest struct {
	// The URL this request was executed for
	URL string

	// Any error received during the request
	Error error

	// The HTTP status code of the request
	StatusCode int

	// The start time
	Started time.Time

	// The time the request has finished
	Finished time.Time

	// The content length
	ContentLength int64

	// HTTP response headers received
	Header Header

	// Stats whether this request reused an established TCP connection via HTTP keep alive or created a fresh one
	Reused bool

	// Timing details for this request
	TimingDetails TimingDetails
}

// ExecuteGet executes a HTTP(S) GET Request for the given URL and returns the result as a Request
func ExecuteGet(url string, header map[string]string, client *http.Client) *FinishedRequest {
	startTime := time.Now()

	req, _ := http.NewRequest("GET", url, nil)
	for name, value := range header {
		req.Header.Add(name, value)
	}

	var dnsStart, dnsDone, connectStart, connectDone, tlsStart, tlsDone, requestStart, headerWritten, ttfbStart, ttfbEnd, transferStart time.Time
	var connectionReused = false

	trace := &httptrace.ClientTrace{
		DNSStart: func(i httptrace.DNSStartInfo) {
			dnsStart = time.Now()
		},

		DNSDone: func(i httptrace.DNSDoneInfo) {
			dnsDone = time.Now()
		},

		ConnectStart: func(_, _ string) {
			connectStart = time.Now()
			if dnsStart.IsZero() {
				dnsStart = connectStart
				dnsDone = connectStart
			}
		},

		ConnectDone: func(network, addr string, err error) {
			connectDone = time.Now()
		},

		TLSHandshakeStart: func() {
			tlsStart = time.Now()
		},

		TLSHandshakeDone: func(_ tls.ConnectionState, _ error) {
			tlsDone = time.Now()
		},

		GotConn: func(i httptrace.GotConnInfo) {
			connectionReused = i.Reused
			requestStart = time.Now()
		},

		WroteRequest: func(info httptrace.WroteRequestInfo) {
			headerWritten = time.Now()
			ttfbStart = headerWritten
		},

		GotFirstResponseByte: func() {
			ttfbEnd = time.Now()
			transferStart = ttfbEnd
		},
	}
	req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))
	resp, err := client.Do(req)

	if err != nil {
		return &FinishedRequest{
			URL:           url,
			Error:         err,
			StatusCode:    -1,
			Started:       startTime,
			Finished:      time.Now(),
			ContentLength: -1,
		}
	}

	defer resp.Body.Close()
	contentLength, err := io.Copy(ioutil.Discard, resp.Body)
	endTime := time.Now()
	transferEnd := endTime

	if err != nil {
		return &FinishedRequest{
			URL:           url,
			Error:         err,
			StatusCode:    resp.StatusCode,
			Started:       startTime,
			Finished:      endTime,
			ContentLength: -1,
			Header:        Header(resp.Header),
			Reused:        connectionReused,
			TimingDetails: TimingDetails{
				TotalTime:           endTime.Sub(startTime),
				DNSTime:             dnsDone.Sub(dnsStart),
				TCPConnectTime:      connectDone.Sub(connectStart),
				TLSTime:             tlsDone.Sub(tlsStart),
				SendRequestTime:     headerWritten.Sub(requestStart),
				TimeToFirstByte:     ttfbEnd.Sub(ttfbStart),
				ContentTransferTime: transferEnd.Sub(transferStart),
			},
		}
	}

	return &FinishedRequest{
		URL:           url,
		Error:         nil,
		StatusCode:    resp.StatusCode,
		Started:       startTime,
		Finished:      endTime,
		ContentLength: contentLength,
		Header:        Header(resp.Header),
		Reused:        connectionReused,
		TimingDetails: TimingDetails{
			TotalTime:           endTime.Sub(startTime),
			DNSTime:             dnsDone.Sub(dnsStart),
			TCPConnectTime:      connectDone.Sub(connectStart),
			TLSTime:             tlsDone.Sub(tlsStart),
			SendRequestTime:     headerWritten.Sub(requestStart),
			TimeToFirstByte:     ttfbEnd.Sub(ttfbStart),
			ContentTransferTime: transferEnd.Sub(transferStart),
		},
	}
}

// TotalTime returns the total time needed to execute the request
func (r *FinishedRequest) TotalTime() time.Duration {
	return r.Finished.Sub(r.Started)
}

// IsError returns whether this sample was successful or not
func (r *FinishedRequest) IsError() bool {
	return r.Error != nil || r.StatusCode > 399
}

// ContentType is a convenience method that returns the content type header value
func (r *FinishedRequest) ContentType() string {
	contentType, ok := r.Header["Content-Type"]
	if !ok {
		return ""
	}

	if len(contentType) == 0 {
		return ""
	}

	return contentType[0]
}
